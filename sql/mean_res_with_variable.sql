set intervals.thickness = 10;
set intervals.minimum = 10;
set intervals.maximum = 20;

SELECT 
	model, 
	"position",
	interval_min,
	interval_max,
	sum(rho_weight) AS mean_res
	FROM (	
		SELECT
			model, 
			"position",
			current_setting('intervals.minimum')::int  AS interval_min,
			current_setting('intervals.maximum')::int AS interval_max,
			CASE
				WHEN depbottom > current_setting('intervals.maximum')::int 
					THEN ((current_setting('intervals.maximum')::int-(depbottom-thickness))/current_setting('intervals.thickness')::int)*rho
				WHEN depbottom-thickness < current_setting('intervals.minimum')::int 
					THEN ((depbottom-current_setting('intervals.minimum')::int)/current_setting('intervals.thickness')::int)*rho
				ELSE (thickness/current_setting('intervals.thickness')::int)*rho
				END AS rho_weight			
		FROM pcgerda.odvlayer
		WHERE depbottom-thickness < current_setting('intervals.maximum')::int AND depbottom > current_setting('intervals.minimum')::int 
			AND model = 644862 AND "position" = 1 --REMOVE FOR FULLSCALE!!!
	) AS tmp
	GROUP BY model, "position", interval_min, interval_max
	ORDER BY model, "position", interval_min
;
			