WITH 
	rho00_10 AS (
		SELECT 
			model, 
			"position",
			0 AS interval_min,
			10 AS interval_max,
			CASE
				WHEN depbottom > 10 
					THEN ((10-(depbottom-thickness))/10)*rho
				WHEN depbottom-thickness < 0
					THEN ((depbottom-0)/10)*rho
				ELSE (thickness/10)*rho
				END AS rho_weighted
		FROM pcgerda.odvlayer
		WHERE depbottom-thickness < 10 AND depbottom > 0
			AND model = 644862 AND "position" = 1
	), 
	rho10_20 AS (
		SELECT 
			model, 
			"position",
			10 AS interval_min,
			20 AS interval_max,
			CASE
				WHEN depbottom > 20 
					THEN ((20-(depbottom-thickness))/10)*rho
				WHEN depbottom-thickness < 10
					THEN ((depbottom-10)/10)*rho
				ELSE (thickness/10)*rho
				END AS rho_weighted			
		FROM pcgerda.odvlayer
		WHERE depbottom-thickness < 20 AND depbottom > 10
			AND model = 644862 AND "position" = 1
	)
SELECT DISTINCT op.model, op."position", interval_min, interval_max, sum(rho00_10.rho_weighted) AS mean_res
FROM pcgerda.odvpos op
INNER JOIN rho00_10 ON op.model = rho00_10.model AND op."position" = rho00_10."position"
GROUP BY op.model, op."position", interval_min, interval_max
UNION
SELECT DISTINCT op.model, op."position", interval_min, interval_max, sum(rho10_20.rho_weighted) AS mean_res
FROM pcgerda.odvpos op
INNER JOIN rho10_20 ON op.model = rho10_20.model AND op."position" = rho10_20."position"
GROUP BY op.model, op."position", interval_min, interval_max
ORDER BY model, "position", interval_min, interval_max