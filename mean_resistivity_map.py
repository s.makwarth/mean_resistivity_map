import os
from functions.db_func import import_db_to_df, export_df_to_db

# set db vars
pghost = "10.33.131.50"
pgport = "5432"
pgdatabase = 'pcgerda'
pguser = os.environ.get('DB_ADMIN_USER')
pgpassword = os.environ.get('DB_ADMIN_PASS')
schemaname = 'dataquality'

interval_thickness = 10
number_of_intervals = 2

for i in range(number_of_intervals):
    interval_min = i*interval_thickness
    interval_max = i*interval_thickness+interval_thickness
    sql = f'''
        set intervals.thickness = {interval_thickness};
        set intervals.minimum = {interval_min};
        set intervals.maximum = {interval_max};
        
        SELECT 
            model, 
            "position",
            interval_min,
            interval_max,
            sum(rho_weight) AS mean_res
        FROM (	
            SELECT
                model, 
                "position",
                current_setting('intervals.minimum')::int  AS interval_min,
                current_setting('intervals.maximum')::int AS interval_max,
                CASE
                    WHEN depbottom > current_setting('intervals.maximum')::int 
                        THEN ((current_setting('intervals.maximum')::int-(depbottom-thickness))/current_setting('intervals.thickness')::int)*rho
                    WHEN depbottom-thickness < current_setting('intervals.minimum')::int 
                        THEN ((depbottom-current_setting('intervals.minimum')::int)/current_setting('intervals.thickness')::int)*rho
                    ELSE (thickness/current_setting('intervals.thickness')::int)*rho
                    END AS rho_weight			
            FROM pcgerda.odvlayer
            WHERE depbottom-thickness < current_setting('intervals.maximum')::int AND depbottom > current_setting('intervals.minimum')::int 
                AND model = 644862 --AND "position" = 1 --REMOVE FOR FULLSCALE!!!
            ) AS tmp
        GROUP BY model, "position", interval_min, interval_max
        ORDER BY model, "position", interval_min
        ;			
    '''
    df = import_db_to_df(pghost, pgport, pgdatabase, pguser, pgpassword, sql)
    tablename = f'mean_resistivity_{interval_min}_{interval_max}'

    export_df_to_db(df, pghost, pgport, pguser, pgpassword, pgdatabase, schemaname, tablename)

